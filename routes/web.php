<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use App\User;

//Blade::setContentTags('[%', '%]');

Route::get('/laravel', function () {

	try
	{
		return User::create([
            'name' => "marcelo",
            'email' => "asdasd@asd.cl",
            'password' => bcrypt("123")
        ]);
	}
	catch(Exception $e){
		return $e;
	}
	
    
});

Route::get('/', function () {
    //return view('ventas/mesasPisoUno');
    return view('login/index');
});





Route::get('/mesasPisoUno', function () {
    return view('ventas/mesasPisoUno');
})->middleware("auth");

Route::get('/mesasPisoDos', function () {
    return view('ventas/mesasPisoDos');
})->middleware("auth");

Route::get("/index2", function(){
	return view("index2");
})->middleware("auth");

Route::get('/editarTragos', function () {
    return view('editarTragos/index');
})->middleware("auth");

Route::get('/ingresarProductos', function () {
    return view('ingresarProductos/index');
})->middleware("auth");

Route::get('/editarInventario', function () {
    return view('editarInventario/index');
})->middleware("auth");

Route::get('/verProducto', function (){
	return view('verProducto/index');
})->middleware("auth");

Route::get('/stock', function () {
    return view('stock/index');
})->middleware("auth");

Route::get('/ventasDiarias', function () {
    return view('ventasDiarias/index');
})->middleware("auth");

Route::get('/verificarInventario', function () {
    return view('verificarInventario/index');
})->middleware("auth");

Route::get('/editarMesas', function () {
    return view('editarMesas/index');
})->middleware("auth");

Route::get('/verificarInventario', function () {
    return view('verificarInventario/index');
})->middleware("auth");

Route::get('/iniciarCaja', function () {
    return view('caja/iniciarCaja');
})->middleware("auth");

Route::get('/cerrarCaja', function () {
    return view('caja/cerrarCaja');
})->middleware("auth");

Route::get('/historialCaja', function () {
    return view('historialCaja/index');
})->middleware("auth");

Route::get("/login", function (){
    return view('login/index');
});


// PRODUCTOS
Route::get('api/v1/productos', 'ProductosController@index');
Route::post('/api/v1/productos', 'ProductosController@store');
Route::delete('/api/v1/productos', 'ProductosController@destroy');
Route::get('api/v1/ingredientes', 'ProductosController@ingredientes');



//TRAGOS
Route::get('api/v1/tragos/{id?}', 'TragosController@index');
Route::post('/api/v1/tragos', 'TragosController@store');
Route::delete('/api/v1/tragos', 'TragosController@destroy');
Route::post('/api/v1/tragos/borrarIngrediente', 'TragosController@borrarIngrediente');

//MESAS
Route::get('api/v1/mesas/{id?}', 'MesasController@index');
Route::post('/api/v1/mesas', 'MesasController@store');
Route::delete('/api/v1/mesas', 'MesasController@destroy');

//STOCK
Route::post('/api/v1/agregarStock', 'StockController@store');
Route::delete('/api/v1/agregarStock/{id}', 'StockController@destroy');
Route::get('/api/v1/verificarInventario', 'StockController@verificarInventario');


//VENTAS
Route::post('/api/v1/ventaMesa', 'VentasController@store');
Route::get('/api/v1/obtenerMesasTragos/{piso}', 'VentasController@obtenerMesasTragos');
Route::delete('/api/v1/ventaMesa', 'VentasController@destroy');
Route::post('/api/v1/emitirBoleta', 'VentasController@emitirBoleta');
//Route::post('/api/v1/login', 'LoginController@login');
Route::get('/home', 'HomeController@index');
Route::get("/api/v1/informeVentasDiarias/{inicio?}/{cierre?}/{producto?}", "VentasController@informeVentasDiarias");
Route::post("/api/v1/cambiarMesa", "VentasController@cambiarMesa");


//CAJA
Route::post("api/v1/iniciarCaja", "CajaController@iniciarCaja");
Route::post("api/v1/cerrarCaja", "CajaController@cerrarCaja");
Route::get("api/v1/historialCaja", "CajaController@historialCaja");


//LOGIN
Route::post("login","UsuarioController@login");
Route::get("logout","UsuarioController@logout");
Route::get("cambiarClave","UsuarioController@cambiarClave");
Route::post("api/v1/cambiarClave","UsuarioController@cambiarClavePost");

