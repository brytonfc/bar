app.controller('ventasPisoUnoController', function($scope, $http, API_URL, $compile, $window,$filter) {
    
    console.log("ventasPisoUnoController");
    $scope.trago                 = {};
    $scope.tragos                = {};
    $scope.tragosMesa            = [];
    //$scope.total                 = 0;
    $scope.mesas                 = []; //$scope.mesas.estado { 0 desocupada | 1 ocupada}
    $scope.mesaSeleccionada      = [];
    $scope.countries             = [];
    $scope.productoSeleccionado  = [];
    $scope.indexMesaSeleccionada = 0;


    
    $http({
        method: 'GET',
        url: API_URL + "obtenerMesasTragos/1",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(response) {
        console.log(response);
        $scope.mesas = response.mesas;


       
    }).error(function(response) {
        console.log(response);
    });


    $http({
        method: 'GET',
        url: API_URL + "tragos",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(response) {
        $scope.tragos = response.tragos; 
        //console.log($scope.tragos);

        angular.forEach($scope.tragos, function(value, key) {
           $scope.countries.push({
            nombre          : value.trago.nombre,
            id_trago        : value.trago.id_trago,
            id_producto     : value.trago.id_producto,
            precio          : value.trago.precio,
            se_vende_unidad : value.trago.se_vende_unidad,
            updated_at      : value.trago.updated_at,
            created_at      : value.trago.created_at,
            categoria       : value.trago.categoria
           })
        });

    }).error(function(response) {
        console.log(response);
    });

    $scope.$watch("productoSeleccionado", function(newValue, oldValue) {
        if( newValue.originalObject == undefined ){
            
        }else{
            $scope.addTrago(newValue.originalObject);
        } 
    });

    $scope.save = function(producto) {
        //var url = API_URL + "ventas";
        producto.id_producto = $scope.productoSeleccionado.originalObject.id_producto;
        
        console.log(producto);
        /*$http({
            method: 'POST',
            url: url,
            data: $.param(producto),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response);
            //$scope.countries = response;
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });*/
    }

    $scope.addTrago = function(trago){
        console.log(trago);
        var ventaMesa = {
            'id_mesa'  : $scope.mesas[$scope.indexMesaSeleccionada].id_mesa,
            "id_trago" : trago.id_trago
        }

        $http({
            method: 'POST',
            url: API_URL + "ventaMesa",
            data: $.param(ventaMesa),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response.id_historial_mesa);
            if(response.resultado == "ok"){
                $scope.mesas[$scope.indexMesaSeleccionada].total = 0;
                $scope.mesas[$scope.indexMesaSeleccionada].tragosMesa.push({
                    "id_historial_mesa" : response.id_historial_mesa,
                    "id_trago"          : trago.id_trago,
                    "nombre"            : trago.nombre,
                    "precio"            : trago.precio,
                    "seleccionado"      : false
                });

                angular.forEach($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa, function(value, key) {
                   $scope.mesas[$scope.indexMesaSeleccionada].total+= value.precio;
                });

                $scope.mesas[$scope.indexMesaSeleccionada].estado = 1;
                console.log($scope.mesas[$scope.indexMesaSeleccionada].total);
            }

        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $scope.deleteTrago = function(tragoMesa){
        console.log(tragoMesa);
        /*var ventaMesa = {
            'id_historial_mesa'  : tragoMesa.id_historial_mesa
        }*/
        $http({
            method: 'DELETE',
            url: API_URL + "ventaMesa",
            data: $.param(tragoMesa),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            if(response.resultado == "ok"){
                $scope.mesas[$scope.indexMesaSeleccionada].total = 0;
                angular.forEach($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa, function(value, key) {
                    if(value.id_historial_mesa == tragoMesa.id_historial_mesa){
                        $scope.mesas[$scope.indexMesaSeleccionada].tragosMesa.splice( key, 1 );
                    }
                });

                angular.forEach($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa, function(value, key) {
                    $scope.mesas[$scope.indexMesaSeleccionada].total+= value.precio;
                });

                if( $scope.mesas[$scope.indexMesaSeleccionada].total == 0)
                    $scope.mesas[$scope.indexMesaSeleccionada].estado = 0;

                console.log($scope.mesas[$scope.indexMesaSeleccionada].total);
            }

        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });

    }

    $scope.mesaSeleccionada = function(index){
        $scope.indexMesaSeleccionada = index;
    }

    $scope.cambiarMesa = function(mesaActual, mesaDestino){
        
        console.log(mesaActual, mesaDestino);
        cambiarMesa = {
            "mesaActual": mesaActual,
            "mesaDestino" : mesaDestino
        }
        $http({
            method: 'POST',
            url: API_URL + "cambiarMesa",
            data: $.param(cambiarMesa),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {

            if(response.resultado == "ok"){
                console.log(response);
                $window.location.reload();
            }

        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });

    }




    $scope.emitirBoleta = function(mesaSeleccionada){
        console.log("emitir boleta...");
        $scope.mesas1 = {};
        $scope.mesas1.tragosMesa = {};
        $scope.mesas1.tragosMesa = $scope.mesas[$scope.indexMesaSeleccionada].tragosMesa;



        $scope.mesas1.nombre = mesaSeleccionada.nombre;
        //console.log("emitir");
        //console.log($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa);
        //console.log($scope.mesas1.tragosMesa);
        /*var ventaMesa = {
            'id_mesa'  : $scope.mesas[$scope.indexMesaSeleccionada].id_mesa
        }*/
        $http({
            method: 'POST',
            url: API_URL + "emitirBoleta",
            data: $.param(mesaSeleccionada),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            //console.log(response);
            if(response.resultado == "ok"){

                /*-------------------------------------------- CREAR BOLETA -----------------------------------*/
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1; //January is 0!
                var yyyy = today.getFullYear();
                if(dd<10){ 
                    dd='0'+dd
                }
                if(mm<10){
                    mm='0'+mm
                } 
                var today = dd+'/'+mm+'/'+yyyy;

                var test1 = [];
                var hola  = [];
                var key2   = 0;
                var totalBoleta =0;
                
                //console.log($scope.mesas1.tragosMesa);
                /*angular.forEach($scope.mesas1.tragosMesa, function(value, key){
                    if(value.seleccionado == true){
                        test1[key2] = {
                            nombre : value.nombre,
                            precio : value.precio
                        };
                        key2++;
                        totalBoleta = totalBoleta + value.precio;

                    }
                });*/
                
               var byName = $scope.mesas1.tragosMesa.slice(0);
               byName.sort(function(a,b) {
                   var x = a.nombre.toLowerCase();
                   var y = b.nombre.toLowerCase();
                   return x < y ? -1 : x > y ? 1 : 0;
               });

               //console.log(byName);
               $scope.mesas1.tragosMesa = byName;

               angular.forEach($scope.mesas1.tragosMesa, function(value, key){
                    
                    if(value.seleccionado == true){
                        cantidad=0;
                        angular.forEach($scope.mesas1.tragosMesa, function(value2, key20){
                            if(value.nombre == value2.nombre){
                                if(value2.seleccionado == true)
                                    cantidad++;
                            }
                        });

                        if(key==0){
                         var auxiliar = value.precio*cantidad;
                         test1[key2] = {
                             NOMBRE : value.nombre,
                             CANT   : cantidad.toString(),
                             //precio : "$"+value.precio*cantidad
                             PRECIO : "$" + $filter('comma2decimal')( $filter('number')(auxiliar) )
                         };
                         key2++;
                         totalBoleta = totalBoleta + value.precio*cantidad;
                         anterior = value.nombre; 

                        }
                        else{
                         if(anterior!=value.nombre){
                             var auxiliar = value.precio*cantidad;
                             test1[key2] = {
                                 NOMBRE : value.nombre,
                                 CANT   : cantidad.toString(),
                                 //precio : "$"+value.precio*cantidad
                                 PRECIO : "$" + $filter('comma2decimal')( $filter('number')(auxiliar) )
                             };
                             key2++;
                             totalBoleta = totalBoleta + value.precio*cantidad;
                             anterior = value.nombre;   
                         }
                         else{
                             anterior = value.nombre;
                         }
                        }
                    }
        
                   
               });
             
          
                


                    
                   

                //console.log($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa);

                test1[key2] = {
                        NOMBRE : "TOTAL",
                        CANT   : "",
                        PRECIO : "$" + $filter('comma2decimal')( $filter('number')(totalBoleta) )
                    } ;
                //console.log(test1);

                
                var propina      = totalBoleta/10;
                var totalPropina = propina + totalBoleta;

                propina          = $filter('comma2decimal')( $filter('number')(propina) );
                totalPropina     = $filter('comma2decimal')( $filter('number')(totalPropina) );

                function buildTableBody(data, columns) {
                    var body = [];

                    body.push(columns);
                    var i=0;
                    data.forEach(function(row) {
                        var dataRow = [];
                        i++;

                        columns.forEach(function(column) {
                            dataRow.push(row[column].toString());
                        })

                        body.push(dataRow);
                        //console.log(dataRow);
                    });
                    
                    return body;
                }

                function table(data, columns) {
                    return {
                        style: 'demoTable',
                        table: {
                            headerRows: 1,
                            widths: [127, 20, 50],
                            body: buildTableBody(data, columns)

                        },
                        layout: 'headerLineOnly',

                        

                    };
                }

                var horaActual = new Date();
                horaActual =    + horaActual.getHours() + ":"  
                                + horaActual.getMinutes() + ":" 
                                + horaActual.getSeconds();
                var dd = {
                    pageSize: 'C7',
                    pageMargins: [ 0, 0, 0, 0 ],
                    content: [
                        {
                            text: "ILICITO BAR\n\n",
                            style: 'nombreBar'
                        },
                        { 
                            text:   $scope.mesas1.nombre + "\n"+
                                    "FECHA : " + today + "\n"+
                                    "HORA  : " + horaActual + "\n\n",
                            style: 'header'
                        },
                        table(test1, ['NOMBRE', 'CANT','PRECIO']),
                        {
                            style: 'demoTable',
                            table: {
                                headerRows: 1,
                                widths: ['*', '*'],
                                body: [
                                    [{text: '', style: 'header'}, {text: '', style: 'header'}],
                                    ["PROP. SUG. 10%", "$" + propina.toString() ],
                                    ["TOTAL C/PROPINA", "$" + totalPropina.toString() ],
                                ]
                            },
                            layout: 'headerLineOnly',
                        },
                        {
                            text:  "\n\nGRACIAS POR PREFERIRNOS",
                            style: "footer"
                        }
                    ],
                    styles: {
                      header: {
                        bold: true,
                        color: '#000',
                        fontSize: 12,
                        alignment: 'left'
                      },
                      demoTable: {
                        color: '#000',
                        fontSize: 11,
                        alignment: 'right',
                      },
                      footer: {
                        bold: true,
                        color: '#000',
                        fontSize: 12,
                        alignment: 'center'
                      },
                      nombreBar: {
                        bold: true,
                        color: '#000',
                        fontSize: 12,
                        alignment: 'center'
                      }
                    }
                }

                
                    
                    

                //pdfMake.createPdf(dd).print();
                //pdfMake.createPdf(dd).print({}, function(){alert();});
                pdfMake.createPdf(dd).getDataUrl(function(url) { 

                    pdfMake.createPdf(dd).print();
                    $http({
                        method: 'GET',
                        url: API_URL + "obtenerMesasTragos/1",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function(response) {
                        //console.log(response);
                        $scope.mesas = response.mesas;


                       
                    }).error(function(response) {
                        console.log(response);
                    });
                    //
                                    
                });
                //location.reload();
                //location.reload();

                //sólo remover los tragos selecionados....
                    //console.log("TRAGOS SELEC");
                    //console.log($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa);
                    /*angular.forEach($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa, function(value, key){            
                        angular.forEach($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa, function(value1, key1){
                            angular.forEach($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa, function(value2, key2){
                                console.log(value2);
                                if(value2.seleccionado == true){
                                    
                                    $scope.mesas[$scope.indexMesaSeleccionada].tragosMesa.splice( key, 1 );
                                }
                            });
                        });
                    });
                    */
    
                    
                    //dejar el total mesa en cero, parcialmente.
                    $scope.mesas[$scope.indexMesaSeleccionada].total = 0;
                    //Calcular el valor de la mesa y asignarlo directemante al total.
                    angular.forEach($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa, function(value, key) {
                        $scope.mesas[$scope.indexMesaSeleccionada].total+= value.precio;
                    });

                    //si el total de la mesa es cer, quiere decir que ya no hay tragos asociados, por lo que la mesa
                    //está desocupada.... (estado = 0)
                    if( $scope.mesas[$scope.indexMesaSeleccionada].total == 0)
                        $scope.mesas[$scope.indexMesaSeleccionada].estado = 0; 
            }

        }).error(function(response) {
            console.log(response);
            //alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $scope.checkOut = function(mesaSeleccionada){
        console.log("emitir boleta...");
        $scope.mesas1 = {};
        $scope.mesas1.tragosMesa = {};
        $scope.mesas1.tragosMesa = $scope.mesas[$scope.indexMesaSeleccionada].tragosMesa;



        $scope.mesas1.nombre = mesaSeleccionada.nombre;
        //console.log("emitir");
        //console.log($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa);
        //console.log($scope.mesas1.tragosMesa);
        /*var ventaMesa = {
            'id_mesa'  : $scope.mesas[$scope.indexMesaSeleccionada].id_mesa
        }*/
        $http({
            method: 'POST',
            url: API_URL + "emitirBoleta",
            data: $.param(mesaSeleccionada),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            //Productos Borrados.
            $window.location.reload();
        }).error(function(response) {
            console.log(response);
            //alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $scope.imprimirBoleta = function(mesaSeleccionada){
        console.log("emitir boleta...");
        $scope.mesas1 = {};
        $scope.mesas1.tragosMesa = {};
        $scope.mesas1.tragosMesa = $scope.mesas[$scope.indexMesaSeleccionada].tragosMesa;



        $scope.mesas1.nombre = mesaSeleccionada.nombre;


                /*-------------------------------------------- CREAR BOLETA -----------------------------------*/
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1; //January is 0!
                var yyyy = today.getFullYear();
                if(dd<10){ 
                    dd='0'+dd
                }
                if(mm<10){
                    mm='0'+mm
                } 
                var today = dd+'/'+mm+'/'+yyyy;

                var test1 = [];
                var hola  = [];
                var key2   = 0;
                var totalBoleta =0;
                
                //console.log($scope.mesas1.tragosMesa);
                /*angular.forEach($scope.mesas1.tragosMesa, function(value, key){
                    if(value.seleccionado == true){
                        test1[key2] = {
                            nombre : value.nombre,
                            precio : value.precio
                        };
                        key2++;
                        totalBoleta = totalBoleta + value.precio;

                    }
                });*/
                
               var byName = $scope.mesas1.tragosMesa.slice(0);
               byName.sort(function(a,b) {
                   var x = a.nombre.toLowerCase();
                   var y = b.nombre.toLowerCase();
                   return x < y ? -1 : x > y ? 1 : 0;
               });

               //console.log(byName);
               $scope.mesas1.tragosMesa = byName;

               angular.forEach($scope.mesas1.tragosMesa, function(value, key){
                    
                    if(value.seleccionado == true){
                        cantidad=0;
                        angular.forEach($scope.mesas1.tragosMesa, function(value2, key20){
                            if(value.nombre == value2.nombre){
                                if(value2.seleccionado == true)
                                    cantidad++;
                            }
                        });

                        if(key==0){
                         var auxiliar = value.precio*cantidad;
                         test1[key2] = {
                             NOMBRE : value.nombre,
                             CANT   : cantidad.toString(),
                             //precio : "$"+value.precio*cantidad
                             PRECIO : "$" + $filter('comma2decimal')( $filter('number')(auxiliar) )
                         };
                         key2++;
                         totalBoleta = totalBoleta + value.precio*cantidad;
                         anterior = value.nombre; 

                        }
                        else{
                         if(anterior!=value.nombre){
                             var auxiliar = value.precio*cantidad;
                             test1[key2] = {
                                 NOMBRE : value.nombre,
                                 CANT   : cantidad.toString(),
                                 //precio : "$"+value.precio*cantidad
                                 PRECIO : "$" + $filter('comma2decimal')( $filter('number')(auxiliar) )
                             };
                             key2++;
                             totalBoleta = totalBoleta + value.precio*cantidad;
                             anterior = value.nombre;   
                         }
                         else{
                             anterior = value.nombre;
                         }
                        }
                    }
        
                   
               });
             
          
                


                    
                   

                //console.log($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa);

                test1[key2] = {
                        NOMBRE : "TOTAL",
                        CANT   : "",
                        PRECIO : "$" + $filter('comma2decimal')( $filter('number')(totalBoleta) )
                    } ;
                //console.log(test1);

                
                var propina      = totalBoleta/10;
                var totalPropina = propina + totalBoleta;

                propina          = $filter('comma2decimal')( $filter('number')(propina) );
                totalPropina     = $filter('comma2decimal')( $filter('number')(totalPropina) );

                function buildTableBody(data, columns) {
                    var body = [];

                    body.push(columns);
                    var i=0;
                    data.forEach(function(row) {
                        var dataRow = [];
                        i++;

                        columns.forEach(function(column) {
                            dataRow.push(row[column].toString());
                        })

                        body.push(dataRow);
                        //console.log(dataRow);
                    });
                    
                    return body;
                }

                function table(data, columns) {
                    return {
                        style: 'demoTable',
                        table: {
                            headerRows: 1,
                            widths: [127, 20, 50],
                            body: buildTableBody(data, columns)

                        },
                        layout: 'headerLineOnly',

                        

                    };
                }

                var horaActual = new Date();
                horaActual =    + horaActual.getHours() + ":"  
                                + horaActual.getMinutes() + ":" 
                                + horaActual.getSeconds();
                var dd = {
                    pageSize: 'C7',
                    pageMargins: [ 0, 0, 0, 0 ],
                    content: [
                        {
                            text: "ILICITO BAR\n\n",
                            style: 'nombreBar'
                        },
                        { 
                            text:   $scope.mesas1.nombre + "\n"+
                                    "FECHA : " + today + "\n"+
                                    "HORA  : " + horaActual + "\n\n",
                            style: 'header'
                        },
                        table(test1, ['NOMBRE', 'CANT','PRECIO']),
                        {
                            style: 'demoTable',
                            table: {
                                headerRows: 1,
                                widths: ['*', '*'],
                                body: [
                                    [{text: '', style: 'header'}, {text: '', style: 'header'}],
                                    ["PROP. SUG. 10%", "$" + propina.toString() ],
                                    ["TOTAL C/PROPINA", "$" + totalPropina.toString() ],
                                ]
                            },
                            layout: 'headerLineOnly',
                        },
                        {
                            text:  "\n\nGRACIAS POR PREFERIRNOS",
                            style: "footer"
                        }
                    ],
                    styles: {
                      header: {
                        bold: true,
                        color: '#000',
                        fontSize: 12,
                        alignment: 'left'
                      },
                      demoTable: {
                        color: '#000',
                        fontSize: 11,
                        alignment: 'right',
                      },
                      footer: {
                        bold: true,
                        color: '#000',
                        fontSize: 12,
                        alignment: 'center'
                      },
                      nombreBar: {
                        bold: true,
                        color: '#000',
                        fontSize: 12,
                        alignment: 'center'
                      }
                    }
                }

                
                    
                    

                //pdfMake.createPdf(dd).print();
                //pdfMake.createPdf(dd).print({}, function(){alert();});
                pdfMake.createPdf(dd).getDataUrl(function(url) { 

                    pdfMake.createPdf(dd).print();
                    $http({
                        method: 'GET',
                        url: API_URL + "obtenerMesasTragos/1",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function(response) {
                        //console.log(response);
                        $scope.mesas = response.mesas;


                       
                    }).error(function(response) {
                        console.log(response);
                    });
                    //
                                    
                });
                //location.reload();
                //location.reload();

                //sólo remover los tragos selecionados....
                    //console.log("TRAGOS SELEC");
                    //console.log($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa);
                    /*angular.forEach($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa, function(value, key){            
                        angular.forEach($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa, function(value1, key1){
                            angular.forEach($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa, function(value2, key2){
                                console.log(value2);
                                if(value2.seleccionado == true){
                                    
                                    $scope.mesas[$scope.indexMesaSeleccionada].tragosMesa.splice( key, 1 );
                                }
                            });
                        });
                    });
                    */
    
                    
                    //dejar el total mesa en cero, parcialmente.
                    $scope.mesas[$scope.indexMesaSeleccionada].total = 0;
                    //Calcular el valor de la mesa y asignarlo directemante al total.
                    angular.forEach($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa, function(value, key) {
                        $scope.mesas[$scope.indexMesaSeleccionada].total+= value.precio;
                    });

                    //si el total de la mesa es cer, quiere decir que ya no hay tragos asociados, por lo que la mesa
                    //está desocupada.... (estado = 0)
                    if( $scope.mesas[$scope.indexMesaSeleccionada].total == 0)
                        $scope.mesas[$scope.indexMesaSeleccionada].estado = 0; 
    }

    $scope.seleccionarTodos = function(){
        angular.forEach($scope.mesas[$scope.indexMesaSeleccionada].tragosMesa, function(itm){ itm.seleccionado = true; });
    }

});