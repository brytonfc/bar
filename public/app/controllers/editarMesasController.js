app.controller('editarMesasController', function($scope, $http, API_URL,SweetAlert,$timeout) {
    
    console.log("editarMesasController");
    
    $scope.nuevaMesa              = {};
    $scope.nuevaMesa.ingredientes = [];
   	$scope.agregado               = false;

   	$scope.textoAgregarEditar      = "Agregar Mesa";

   	//rescatar todas las mesas
   	var url = API_URL + "mesas";
   	$http({
        method: 'GET',
        url: url,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(response) {
        console.log(response.mesas);
        $scope.mesas = response.mesas;

    }).error(function(response) {
        console.log(response);
        //alert('This is embarassing. An error has occured. Please check the log for details');
    });


    $scope.save = function(nuevaMesa){
    	console.log(nuevaMesa);

        $http({
            method: 'POST',
            url: API_URL + "mesas",
            data : $.param(nuevaMesa),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response);

            if(response.resultado == "ok"){
            	$timeout(function () { $scope.agregado = false; }, 3000);
	            $scope.agregado = true;
	            $scope.edicion = false;
	            $scope.nuevaMesa              = {};
	            
	            if(response.accion == "guardar"){
	                $scope.mesas.push({ 
	                    "id_mesa" : response.id_mesa,
	                    "nombre"  : nuevaMesa.nombre,
	                    "piso"    : nuevaMesa.piso
	                });    
	            }

	            if(response.accion == "editar"){
	                angular.forEach($scope.mesas, function(value, key) {
	                    if(value.id_mesa == nuevaMesa.id_mesa){
	                        value.nombre = nuevaMesa.nombre;
	                        value.piso   = nuevaMesa.piso;
	                    }
	                });   
	            }

	            $scope.textoAgregarEditar      = "Agregar Mesa";
            }

        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $scope.editar = function(mesa){
        console.log(mesa);
		$scope.nuevaMesa.nombre   = mesa.nombre;
		$scope.nuevaMesa.piso     = parseInt(mesa.piso);
		$scope.nuevaMesa.id_mesa  = parseInt(mesa.id_mesa);
		$scope.textoAgregarEditar = "Editar Mesas";
		$scope.edicion            = true;
    }

    $scope.delete = function(mesa) {
        console.log(mesa);
        var url = API_URL + "mesas";

        SweetAlert.swal({   title: "Estas seguro?",
           text: "No podras recuperar este registro",
           type: "warning",   
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",   
           confirmButtonText: "Borrar"  
           //closeOnConfirm: false 
            }, 
           function(isConfirm){   
            console.log(isConfirm);
            if(isConfirm){
                $http({
                    method: 'DELETE',
                    url: url,
                    data: $.param(mesa),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(response) {
                    console.log(response.resultado);
                    if( response.resultado == "ok" ){
                        angular.forEach($scope.mesas, function(value, key) {
                            if(value.id_mesa == mesa.id_mesa){
                                $scope.mesas.splice( key, 1 );
                            }
                        });   
                    }    
                }).error(function(response) {
                    console.log(response);
                    alert('This is embarassing. An error has occured. Please check the log for details');
                });

            }
        });
       
    }
});