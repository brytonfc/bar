app.controller('historialCajaController', function($scope, $http, API_URL,$timeout,SweetAlert) {
    
    console.log("historialCajaController");
    $scope.mostrarDetalle       = false;
    $scope.productos            = {};
    $scope.agregado             = false;
    $scope.textoAgregarEditar   = "Agregar al inventario";
    $scope.producto             = {};
    $scope.producto.nombre      = "";
    $scope.producto.id_producto = "";

    //SweetAlert.swal("Here's a message");
    
    
    var url = API_URL + "historialCaja";
    $http({
        method: 'GET',
        url: url,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(response) {
        console.log(response);
        $scope.caja = response.cajas;

    }).error(function(response) {
        console.log(response);
        alert('This is embarassing. An error has occured. Please check the log for details');
    });

    
    $scope.detalle = function(item){
        $scope.mostrarDetalle = true;
        $scope.detalle.inicio  = item.inicio ;
        $scope.detalle.cierre  = item.cierre ;
        $scope.detalle.inicial = item.valor ;
        $scope.detalle.ventas  = item.ventas ;
        $scope.detalle.dia     = item.sumaTotal ;
    }
});