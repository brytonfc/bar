app.controller('cerrarCajaController', function($scope, $http, API_URL,$timeout,SweetAlert) {
    
    console.log("cerrarCajaController");
    $scope.producto             = {};
    $scope.countries            = [];
    $scope.productoSeleccionado = {};
    $scope.cerrarOk = false;
    $scope.edicion = true;

    $scope.save = function() {
        var url = API_URL + "cerrarCaja";

        $http({
            method: 'POST',
            url: url,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response);
            if( response.resultado == "ok" ){
                $scope.datos = response.datos;
                $scope.cerrarOk = true;
                $timeout(function () { 
                    $scope.cerrarOk = false; 
                }, 3000); 
            }else{
                SweetAlert.swal(response.mensaje);
            }
            //$scope.countries = response;
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

});