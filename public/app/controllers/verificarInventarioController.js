app.controller('verificarInventarioController', function($scope, $http, API_URL,$timeout,SweetAlert) {
    
    console.log("verificarInventarioController");
    $scope.productos            = {};
    $scope.agregado             = false;
    $scope.textoAgregarEditar   = "Agregar al inventario";
    $scope.producto             = {};
    $scope.producto.nombre      = "";
    $scope.producto.id_producto = "";

    //SweetAlert.swal("Here's a message");
    
    
    var url = API_URL + "verificarInventario";
    $http({
        method: 'GET',
        url: url,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(response) {
        console.log(response);
        $scope.stock = response;

    }).error(function(response) {
        console.log(response);
        alert('This is embarassing. An error has occured. Please check the log for details');
    });

    $scope.save = function(producto) {
        console.log(producto);
        var url = API_URL + "productos";
       
        $http({
            method: 'POST',
            url: url,
            data: $.param(producto),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response.resultado);
            if( response.resultado == "ok" ){
                $timeout(function () { $scope.agregado = false; }, 3000);
                $scope.agregado           = true;
                $scope.producto           = {};
                
                if(response.accion == "guardar"){
                    $scope.productos.push({ 
                        "id_producto" : response.id_producto,
                        "nombre"      : producto.nombre
                    });    
                }

                if(response.accion == "editar"){
                    angular.forEach($scope.productos, function(value, key) {
                        if(value.id_producto == producto.id_producto){
                            value.nombre = producto.nombre;
                        }
                    });   
                }
                
                $scope.edicion            = false;
                $scope.textoAgregarEditar = "Agregar al inventario";
            }
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
        
    }

    $scope.editar = function(producto){
        console.log(producto);
        $scope.producto.nombre      = producto.nombre;
        $scope.producto.id_producto = producto.id_producto;
        $scope.textoAgregarEditar   = "Editar inventario";
        $scope.edicion              = true;
    }

    $scope.delete = function(producto) {
        console.log(producto);
        var url = API_URL + "productos";
       
        $http({
            method: 'DELETE',
            url: url,
            data: $.param(producto),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response.resultado);
            if( response.resultado == "ok" ){
                angular.forEach($scope.productos, function(value, key) {
                    if(value.id_producto == producto.id_producto){
                        $scope.productos.splice( key, 1 );
                    }
                });   
            }    
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
        
    }
});