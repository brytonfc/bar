app.controller('iniciarCajaController', function($scope, $http, API_URL,$timeout) {
    
    console.log("iniciarCajaController");
    $scope.datos             = {};
    $scope.iniciarOk = false;
    $scope.edicion = true;

    $scope.save = function(producto) {
        var url = API_URL + "iniciarCaja";

        console.log(producto);
        $http({
            method: 'POST',
            url: url,
            data: $.param(producto),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            $scope.iniciarOk = true;
            $timeout(function () { 
                $scope.iniciarOk = false; 
            }, 3000);
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

});