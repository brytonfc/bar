app.controller('ingresarProductosController', function($scope, $http, API_URL,$timeout) {
    
    console.log("ingresarProductosController");
    $scope.producto             = {};
    $scope.countries            = [];
    $scope.productoSeleccionado = {};
    $scope.edicion              = false;
    $scope.textoOk              = "";

    $http({
        method: 'GET',
        url: API_URL + "productos",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(response) {
        $scope.countries = response;
    }).error(function(response) {
        console.log(response);
        alert('This is embarassing. An error has occured. Please check the log for details');
    });

    $scope.save = function(producto) {
        var url = API_URL + "agregarStock";
        producto.id_producto = $scope.productoSeleccionado.originalObject.id_producto;
        
        console.log(producto);
        $http({
            method: 'POST',
            url: url,
            data: $.param(producto),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            $scope.edicion = true;
            $scope.textoOk = "(producto ingresado al stock)";
            $timeout(function () { 
                $scope.textoOk = "";
                $scope.edicion = false; 
            }, 3000);
            $scope.producto = {};
            console.log(response);
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

});