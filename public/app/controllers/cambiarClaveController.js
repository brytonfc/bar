app.controller('cambiarClaveController', function($scope, $http, API_URL,$timeout) {
    
    console.log("cambiarClaveController");
    $scope.datos             = {};
    $scope.iniciarOk = false;
    $scope.edicion = true;


    //console.log($scope.datos.nuevaClave);

    $scope.change = function(){
        if($scope.datos.nuevaClave == $scope.datos.repitaClave){
            console.log("ok");
            $scope.textoPassword ="";
        }
        else
        {
            console.log("NO");
            $scope.textoPassword ="Contraseña no coincide";

        }
        //console.log($scope.datos.nuevaClave);
    }

    $scope.save = function(producto) {
        var url = API_URL + "cambiarClave";

        console.log(producto);
        $http({
            method: 'POST',
            url: url,
            data: $.param(producto),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            $scope.iniciarOk = true;
            $timeout(function () { 
                $scope.iniciarOk = false; 
            }, 3000);
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

});