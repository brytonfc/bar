app.controller('editarTragosController', function($scope, $http, API_URL,SweetAlert, $sce, $window) {
    
    console.log("editarTragosController");

    $scope.nuevoTrago              = {};
    $scope.nombreProductos         = [];
    $scope.nuevoTrago.ingredientes = [];
    $scope.productos               = [];
   	$scope.editarTrago             = "";
    $scope.agregar                 = true;

    $scope.producto                = {};
    $scope.producto.nombre         = "";
    $scope.producto.id_producto    = "";
    $scope.ingedienteNuevo         = "";

   	$scope.textoAgregarEditar      = "Agregar Trago";
    $scope.esCerveza = false;

   	//rescatar todos los tragos
   	var url = API_URL + "tragos";
   	$http({
        method: 'GET',
        url: url,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(response) {
        console.log(response.tragos);
        $scope.tragos = response.tragos;

    }).error(function(response) {
        console.log(response);
        //alert('This is embarassing. An error has occured. Please check the log for details');
    });

    $http({
        method: 'GET',
        url: API_URL + "ingredientes",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(response) {        
        console.log(response);
        $scope.productos    = response.ingredientes;
        
        angular.forEach($scope.productos, function(value, key) {
            $scope.nombreProductos.push(value.nombre);
        });


    }).error(function(response) {
        console.log(response);
        alert('This is embarassing. An error has occured. Please check the log for details');
    });



    $scope.save = function(nuevoTrago){
    	console.log(nuevoTrago);

        $http({
            method: 'POST',
            url: API_URL + "tragos",
            data : $.param(nuevoTrago),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response);
            $scope.edicion = false;
            $scope.nuevoTrago              = {};
            $scope.nuevoTrago.ingredientes = {};
            $scope.textoAgregarEditar      = "Agregar Trago";
            //$scope.ingredientes            = $scope.productos;
            $scope.agregar                 = true;

            angular.forEach($scope.tragos, function(value, key) {
                if(value.trago.id_trago == nuevoTrago.id_trago){
                    console.log("trago a reemplazar",value);
                    console.log("nuevo trago", nuevoTrago);
                    value.trago.nombre =  nuevoTrago.nombre;
                    value.trago.valor  =  nuevoTrago.valor;
                    value.ingredientes =  $scope.ingredientesTrago;
                }
            });
            
            $window.location.reload();
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $scope.nuevoIngrediente = function(){
        //console.log("a");
    	$scope.nuevoTrago.ingredientes.push({ 
            "ingrediente": ""
        });
    }

    $scope.editar = function(trago){
        console.log(trago);
        $scope.textoAgregarEditar      = "Editar inventario";
        $scope.edicion                 = true;
        $scope.agregar                 = false;
        
        $scope.nuevoTrago.nombre       = trago.trago.nombre;
        $scope.nuevoTrago.valor        = trago.trago.precio;
        $scope.nuevoTrago.id_trago     = trago.trago.id_trago;    
        $scope.nuevoTrago.ingredientes = [];
        
        $scope.ingredientesTrago       = trago.ingredientes;
        
        console.log(trago.ingredientes);
        if( ($scope.ingredientesTrago) && ($scope.ingredientesTrago.length == 0) ){
            //alert("vacio");
            $scope.esCerveza = true;
        }else{
            $scope.esCerveza = false;
        }
    }

    $scope.delete = function(trago) {
        console.log(trago.trago.id_trago);
        var url = API_URL + "tragos";

        SweetAlert.swal({   title: "Estas seguro?",
           text: "No podras recuperar este registro",  
           type: "warning",   
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",   
           confirmButtonText: "Borrar"  
           //closeOnConfirm: false 
            }, 
           function(isConfirm){   
            console.log(isConfirm);
            if(isConfirm){
                $http({
                    method: 'DELETE',
                    url: url,
                    data: $.param(trago),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(response) {
                    console.log(response.resultado);
                    if( response.resultado == "ok" ){
                        angular.forEach($scope.tragos, function(value, key) {
                            if(value.trago.id_trago == trago.trago.id_trago){
                                $scope.tragos.splice( key, 1 );
                            }
                        });   
                    }else{
                        sweetAlert("Oops...", response.mensaje, "error");
                    }  
                }).error(function(response) {
                    console.log(response);
                    alert('This is embarassing. An error has occured. Please check the log for details');
                });

            }
            //swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
        });

        /*
        */
        
    }

    $scope.deleteIngrediente = function(ingrediente){
        $http({
            method: 'POST',
            url: API_URL + "tragos/borrarIngrediente",
            data : $.param(ingrediente),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response);
            //borrar el ingrediente de la vista....
            angular.forEach($scope.ingredientesTrago, function(value, key) {
                if( value.id_ingredientes_trago == ingrediente.id_ingredientes_trago){
                    $scope.ingredientesTrago.splice(key, 1);   
                }
            });
            
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

});

