app.controller('ventasDiariasController', function($scope, $http, API_URL,$timeout,SweetAlert) {
    
    console.log("ventasDiariasController");
    $scope.productos            = {};
    $scope.agregado             = false;
    $scope.textoAgregarEditar   = "Agregar al inventario";
    $scope.producto             = {};
    $scope.producto.nombre      = "";
    $scope.producto.id_producto = "";

    //SweetAlert.swal("Here's a message");
    

    
    var url = API_URL + "informeVentasDiarias";
    $http({
        method: 'GET',
        url: url,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(response) {
        console.log(response);
        $scope.stock = response.datos;
        $scope.sumaTotal = response.suma;

    }).error(function(response) {
        console.log(response);
        alert('This is embarassing. An error has occured. Please check the log for details');
    });

    $http({
        method: 'GET',
        url: API_URL + "tragos",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(response) {
        console.log(response.tragos);
        $scope.countries = response.tragos;
    }).error(function(response) {
        console.log(response);
        alert('This is embarassing. An error has occured. Please check the log for details');
    });

    $scope.filtro = function(ventas){
        console.log(ventas);

        desde = null;
        hasta = null;
        producto = null;
        if(typeof ventas != 'undefined'){
            if(ventas.hasOwnProperty("desde")){
               desde = ventas.desde; 
            }

            if(ventas.hasOwnProperty("hasta")){
               hasta = ventas.hasta; 
            }

            if(ventas.hasOwnProperty("productoSeleccionado")){
                if(typeof ventas.productoSeleccionado != 'undefined'){
                    producto =  ventas.productoSeleccionado.originalObject.trago.id_trago; 
  
                }
            }

        }


        
       
 
        var url = API_URL + "informeVentasDiarias/"+desde+"/"+hasta+"/"+producto;
        $http({
            method: 'GET',
            url: url,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(response) {
            console.log(response.resultado);
            $scope.stock = response.datos;
            $scope.sumaTotal = response.suma;
        }).error(function(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }
});