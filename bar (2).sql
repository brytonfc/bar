-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 21-12-2016 a las 20:29:27
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja`
--

CREATE TABLE `caja` (
  `id_caja` int(11) NOT NULL,
  `fecha_inicio_caja` datetime NOT NULL,
  `fecha_cierre_caja` datetime DEFAULT NULL,
  `valor` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `caja`
--

INSERT INTO `caja` (`id_caja`, `fecha_inicio_caja`, `fecha_cierre_caja`, `valor`, `created_at`, `updated_at`) VALUES
(1, '2016-12-01 17:37:20', '2016-12-20 18:14:20', 1200, '2016-12-20 17:37:20', '2016-12-20 18:14:20'),
(2, '2016-12-21 10:30:00', '2016-12-21 10:30:14', 5000, '2016-12-21 10:30:00', '2016-12-21 10:30:14'),
(3, '2016-12-21 10:32:38', '2016-12-21 10:32:46', 10000, '2016-12-21 10:32:38', '2016-12-21 10:32:46'),
(4, '2016-12-21 10:40:26', '2016-12-21 10:40:30', 1, '2016-12-21 10:40:26', '2016-12-21 10:40:30'),
(5, '2016-12-21 10:41:15', '2016-12-21 10:41:19', 1, '2016-12-21 10:41:15', '2016-12-21 10:41:19'),
(6, '2016-12-21 10:42:06', '2016-12-21 10:42:10', 1, '2016-12-21 10:42:06', '2016-12-21 10:42:10'),
(7, '2016-12-21 10:42:49', '2016-12-21 10:42:54', 1, '2016-12-21 10:42:49', '2016-12-21 10:42:54'),
(8, '2016-12-21 10:43:16', '2016-12-21 10:43:22', 1500, '2016-12-21 10:43:16', '2016-12-21 10:43:22'),
(9, '2016-12-21 10:51:29', '2016-12-21 10:51:35', 5000, '2016-12-21 10:51:29', '2016-12-21 10:51:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_anadir_stock`
--

CREATE TABLE `historial_anadir_stock` (
  `id_historial_anadir_stock` int(11) NOT NULL,
  `id_stock` int(11) NOT NULL,
  `ml` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_mesa`
--

CREATE TABLE `historial_mesa` (
  `id_historial_mesa` int(11) NOT NULL,
  `id_trago` int(11) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `seleccionado` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `historial_mesa`
--

INSERT INTO `historial_mesa` (`id_historial_mesa`, `id_trago`, `id_mesa`, `seleccionado`, `created_at`, `updated_at`) VALUES
(229, 83, 4, 0, '2016-12-21', '2016-12-21'),
(230, 83, 4, 0, '2016-12-21', '2016-12-21'),
(231, 83, 4, 0, '2016-12-21', '2016-12-21'),
(232, 83, 4, 0, '2016-12-21', '2016-12-21'),
(233, 83, 4, 0, '2016-12-21', '2016-12-21'),
(234, 83, 4, 0, '2016-12-21', '2016-12-21'),
(235, 84, 7, 0, '2016-12-21', '2016-12-21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_venta`
--

CREATE TABLE `historial_venta` (
  `id_historial_venta` int(11) NOT NULL,
  `id_stock` int(11) NOT NULL,
  `id_trago` int(11) NOT NULL,
  `ml` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `historial_venta`
--

INSERT INTO `historial_venta` (`id_historial_venta`, `id_stock`, `id_trago`, `ml`, `precio`, `created_at`, `updated_at`) VALUES
(1, 19, 77, 0, 1000, '2016-12-20 00:00:00', '2016-12-20'),
(2, 20, 78, 0, 2000, '2016-12-20 00:00:00', '2016-12-20'),
(3, 21, 78, 0, 3000, '2016-12-20 00:00:00', '2016-12-20'),
(4, 22, 78, 0, 4000, '2016-12-20 00:00:00', '2016-12-20'),
(5, 25, 77, 1, 1500, '2016-12-20 00:00:00', '2016-12-20'),
(6, 26, 78, 100, 1500, '2016-12-20 00:00:00', '2016-12-20'),
(7, 27, 77, 0, 1500, '2016-12-13 16:00:40', '2016-12-20'),
(8, 28, 77, 0, 1500, '2016-12-31 16:00:40', '2016-12-20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingredientes_trago`
--

CREATE TABLE `ingredientes_trago` (
  `id_ingredientes_trago` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_trago` int(11) NOT NULL,
  `ml` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ingredientes_trago`
--

INSERT INTO `ingredientes_trago` (`id_ingredientes_trago`, `id_producto`, `id_trago`, `ml`, `created_at`, `updated_at`) VALUES
(21, 39, 83, 0, '2016-12-21', '2016-12-21'),
(22, 38, 84, 200, '2016-12-21', '2016-12-21'),
(23, 39, 84, 500, '2016-12-21', '2016-12-21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

CREATE TABLE `mesa` (
  `id_mesa` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `piso` varchar(2) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mesa`
--

INSERT INTO `mesa` (`id_mesa`, `nombre`, `piso`, `created_at`, `updated_at`) VALUES
(4, 'Mesa 1', '1', '2016-10-27', '2016-10-28'),
(7, 'Mesa 2', '1', '2016-10-27', '2016-10-28'),
(8, 'Mesa 3', '1', '2016-10-28', '2016-10-28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id_producto` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `se_vende_unidad` tinyint(1) DEFAULT '0',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id_producto`, `nombre`, `se_vende_unidad`, `created_at`, `updated_at`) VALUES
(38, 'Pisco', 0, '2016-12-21', '2016-12-21'),
(39, 'Bebida 1 Lt', 1, '2016-12-21', '2016-12-21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock`
--

CREATE TABLE `stock` (
  `id_stock` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `ml` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `stock`
--

INSERT INTO `stock` (`id_stock`, `id_producto`, `ml`, `cantidad`, `created_at`, `updated_at`) VALUES
(67, 38, 200, -1, '2016-12-21', '2016-12-21'),
(68, 39, 1, -1, '2016-12-21', '2016-12-21'),
(69, 39, 1, -1, '2016-12-21', '2016-12-21'),
(70, 39, 1, -1, '2016-12-21', '2016-12-21'),
(71, 39, 1, -1, '2016-12-21', '2016-12-21'),
(72, 39, 1, -1, '2016-12-21', '2016-12-21'),
(73, 38, 750, 1, '2016-12-21', '2016-12-21'),
(74, 39, 500, 10, '2016-12-21', '2016-12-21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trago`
--

CREATE TABLE `trago` (
  `id_trago` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `precio` int(11) NOT NULL,
  `categoria` varchar(50) DEFAULT NULL,
  `se_vende_unidad` tinyint(1) NOT NULL DEFAULT '0',
  `id_producto` int(11) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `trago`
--

INSERT INTO `trago` (`id_trago`, `nombre`, `precio`, `categoria`, `se_vende_unidad`, `id_producto`, `created_at`, `updated_at`) VALUES
(83, 'Bebida 1 Lt', 1200, NULL, 1, 39, '2016-12-21', '2016-12-21'),
(84, 'Piscola', 2000, NULL, 0, NULL, '2016-12-21', '2016-12-21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `password`, `name`, `email`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '$2y$10$YwqTg65c1Qb142YwhqA8ju8E4hHP1kP4TD2O6.83lPGlHfwvq00Ia', 'fabian', 'fasme@asd.cl', NULL, '2016-10-25', '2016-10-25');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `caja`
--
ALTER TABLE `caja`
  ADD PRIMARY KEY (`id_caja`);

--
-- Indices de la tabla `historial_anadir_stock`
--
ALTER TABLE `historial_anadir_stock`
  ADD PRIMARY KEY (`id_historial_anadir_stock`),
  ADD KEY `fk_stock` (`id_stock`);

--
-- Indices de la tabla `historial_mesa`
--
ALTER TABLE `historial_mesa`
  ADD PRIMARY KEY (`id_historial_mesa`),
  ADD KEY `fk_trago` (`id_trago`),
  ADD KEY `fk_mesa` (`id_mesa`);

--
-- Indices de la tabla `historial_venta`
--
ALTER TABLE `historial_venta`
  ADD PRIMARY KEY (`id_historial_venta`),
  ADD KEY `fk_stock` (`id_stock`),
  ADD KEY `fk_trago` (`id_trago`);

--
-- Indices de la tabla `ingredientes_trago`
--
ALTER TABLE `ingredientes_trago`
  ADD PRIMARY KEY (`id_ingredientes_trago`),
  ADD KEY `fk_trago` (`id_trago`),
  ADD KEY `fk_producto` (`id_producto`);

--
-- Indices de la tabla `mesa`
--
ALTER TABLE `mesa`
  ADD PRIMARY KEY (`id_mesa`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id_stock`),
  ADD KEY `fk_prioducto` (`id_producto`);

--
-- Indices de la tabla `trago`
--
ALTER TABLE `trago`
  ADD PRIMARY KEY (`id_trago`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `caja`
--
ALTER TABLE `caja`
  MODIFY `id_caja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `historial_anadir_stock`
--
ALTER TABLE `historial_anadir_stock`
  MODIFY `id_historial_anadir_stock` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `historial_mesa`
--
ALTER TABLE `historial_mesa`
  MODIFY `id_historial_mesa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;
--
-- AUTO_INCREMENT de la tabla `historial_venta`
--
ALTER TABLE `historial_venta`
  MODIFY `id_historial_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `ingredientes_trago`
--
ALTER TABLE `ingredientes_trago`
  MODIFY `id_ingredientes_trago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `mesa`
--
ALTER TABLE `mesa`
  MODIFY `id_mesa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT de la tabla `stock`
--
ALTER TABLE `stock`
  MODIFY `id_stock` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT de la tabla `trago`
--
ALTER TABLE `trago`
  MODIFY `id_trago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
