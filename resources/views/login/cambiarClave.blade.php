@extends('layouts.template');


@section('content')
        
    <div class="container-fluid" ng-controller="cambiarClaveController">
            <div class="block-header">
                <h2>Cambiar clave</h2>
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">

                        
                        <div class="body">
                            <form>
                                <div class="form-group">
                                    <label for="email">Nueva clave:</label>
                                    <input ng-change="change()" ng-model="datos.nuevaClave" type="text" class="form-control" placeholder="Nueva clave" style="background-color: #e6e6e6;">
                                </div>
                                
                                <div class="form-group">
                                    <label for="email">Repita clave:</label>
                                    <input ng-change="change()" ng-model="datos.repitaClave" type="text" class="form-control" placeholder="Repita clave" style="background-color: #e6e6e6;">
                                </div>

                                <div>[[textoPassword]]</div>

                                <button ng-if="textoPassword==''" ng-click="save(datos)" class="btn btn-default">Cambiar</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                
                <!-- #END# Browser Usage -->
            </div>
        </div>

@endsection
</html>