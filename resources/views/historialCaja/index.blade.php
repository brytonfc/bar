@extends('layouts.template');


@section('content')
        
    <div class="container-fluid" ng-controller="historialCajaController">
            <div class="block-header">
                <h2>Historial Caja</h2>
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header">

                            <h2>Cantidad productos</h2>

                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Inicio</th>
                                            <th>Cierre</th>
                                            <th>Detalle</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <tr ng-repeat="item in caja">
                                            <td>[[$index+1]]</td>
                                            <td>[[item.inicio]]</td>
                                            <td>[[item.cierre]]</td>
                                            <td>
                                                <i ng-click="detalle(item)" class="material-icons col-light-blue">mode_edit</i>
                                            </td>
                                        </tr>  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" ng-show="mostrarDetalle">
                    <div class="card">
                         <h2>Detalle caja <strong><small>( [[detalle.inicio]] - [[detalle.cierre]] )</small></strong> </h2>
                        <div class="body">
                            <form>
                                <div class="form-group">
                                    <label for="email">Caja incial: $[[detalle.inicial |number|comma2decimal]]</label>                                    
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Total ventas: $[[detalle.ventas |number|comma2decimal]]</label>    
                                </div>
                                <div class="form-group">
                                    <label for="sel1">Total día: $[[detalle.dia |number|comma2decimal]]</label>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                
                <!-- #END# Browser Usage -->
            </div>
        </div>

@endsection
</html>