@extends('layouts.template');


@section('content')
        
    <div class="container-fluid" ng-controller="ventasDiariasController">
            <div class="block-header">
                <h2>Ventas diarias</h2>
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header">

                            <h2>Cantidad ventas</h2>

                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>nombre</th>
                                            <th>Precio</th>
                                            <th>Fecha venta</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <tr ng-repeat="item in stock">
                                            <td>[[$index+1]]</td>
                                            <td>[[item.nombre]]</td>
                                            <td>$[[item.precio |number|comma2decimal]]</td>
                                            <td>[[item.fecha]]</td>
                                        </tr>  
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                        <td></td>
                                        <td>Total:</td>
                                        <td>$[[sumaTotal |number|comma2decimal]]</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                         <h2>Filtro Fechas</h2>
                        <div class="body">
                            <form>




                                <div class="form-group">
                                    <label for="desde">desde</label>
                                    <input ng-model="venta.desde" type="date" class="form-control" placeholder="Desde" style="background-color: #e6e6e6;">                                    
                                </div>
                                
                                <div class="form-group">
                                    <label for="hasta">hasta</label>
                                    <input ng-model="venta.hasta" type="date" class="form-control" placeholder="Hasta" style="background-color: #e6e6e6;">  
                                </div>


                                <angucomplete-alt id="ex1"
                                  placeholder="buscar producto"
                                  pause="100"
                                  selected-object="venta.productoSeleccionado"
                                  local-data="countries"
                                  search-fields="trago.nombre"
                                  title-field="trago.nombre"
                                  minlength="1"
                                  input-class="form-control form-control-small" >
                                  </angucomplete-alt>
                                
                                <div class="form-group">
                                    <button ng-click="filtro(venta)" class="btn btn-default">Filtrar</button>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>

                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                
                <!-- #END# Browser Usage -->
            </div>
        </div>

@endsection
</html>