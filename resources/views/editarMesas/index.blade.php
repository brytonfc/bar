@extends('layouts.template');


@section('content')
        
    <div class="container-fluid" ng-controller="editarMesasController">
            <div class="block-header">
                <h2>Editar Mesas</h2>
            </div>

           
            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>Lista de mesas</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>#</th>

                                            <th>Piso</th>
                                            <th>Titulo</th>
                                            <th>Nombre</th>
                                            <th>Editar</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <tr ng-repeat="mesa in mesas">
                                            <td>[[$index+1]]</td>
                                            <td>[[mesa.piso]]</td>
                                            <td>[[mesa.titulo]]</td>
                                            <td>[[mesa.nombre]]</td>
                                            <td>
                                                <i ng-click="editar(mesa)" class="material-icons col-light-blue">mode_edit</i>
                                                <i ng-click="delete(mesa)" class="material-icons col-red">delete</i>
                                            </td>
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="alert bg-green" ng-show="agregado">
                        Mesa agregada/editada, correctamente.
                    </div>
                    <div class="card">
                        <div class="header" ng-class="(edicion==true) ? 'bg-deep-orange' : 'bg-teal'">
                            <h2>[[textoAgregarEditar]]</h2>
                        </div>
                        <div class="body">
                            <form>
                                <div class="form-group">
                                    <label for="email">Nombre Mesa:</label>
                                    <input ng-model="nuevaMesa.nombre" type="text" class="form-control" placeholder="Nombre" style="background-color: #e6e6e6;">
                                </div>

                                <div class="form-group">
                                    <label for="pwd">Piso:</label>
                                    <input ng-model="nuevaMesa.piso" type="number" placeholder="Piso" class="form-control" id="pwd" style="background-color: #e6e6e6;">
                                </div>

                                <div class="form-group">
                                    <label for="pwd">Titulo:</label>
                                    <input ng-model="nuevaMesa.titulo" type="text" placeholder="Ej: T1" class="form-control" id="titulo" style="background-color: #e6e6e6;">
                                </div>
        
                                <button ng-click="save(nuevaMesa)" class="btn btn-default">[[textoAgregarEditar]]</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- #END# Browser Usage -->
            </div>
        </div>

@endsection
</html>