@extends('layouts.template');


@section('content')

    <div class="container-fluid" ng-controller="ingresarProductosController">
            <div class="block-header">
                <h2>Ingresar Stock</h2>
            </div>

            <!-- #END# Widgets -->
            <!-- CPU Usage -->
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header"  ng-class="(edicion==true) ? 'bg-deep-orange' : 'bg-teal'">
                            <h2>Ingresar Stock [[textoOk]]</h2>                         
                        </div>
                        <div class="body">
                            <div class="form-group">
                                <label for="email">Nombre producto:</label>
                                <!--<input ng-model="producto.nombre" type="text" class="form-control" placeholder="Nombre" style="background-color: #e6e6e6;">-->
                                <angucomplete-alt id="ex1"
                                  placeholder="buscar producto"
                                  pause="100"
                                  selected-object="productoSeleccionado"
                                  local-data="countries"
                                  search-fields="nombre"
                                  title-field="nombre"
                                  minlength="1"
                                  input-class="form-control form-control-small"/>
                                  
                            </div>

                            <div class="form-group">
                                <label for="email">Cantidad:</label>
                                <input ng-model="producto.cantidad" type="number" class="form-control" placeholder="cantidad" style="background-color: #e6e6e6;">
                            </div>

                            <div class="form-group">
                                <label for="email">ML:</label>
                                <input ng-model="producto.ml" type="text" class="form-control" placeholder="Miligramos" style="background-color: #e6e6e6;">
                            </div>
                            <div class="form-group">
                                <button ng-click="save(producto)" class="btn btn-default">Agregar Stock</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# CPU Usage -->

@endsection
</html>