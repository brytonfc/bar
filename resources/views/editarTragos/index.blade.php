@extends('layouts.template');


@section('content')
        
    <div class="container-fluid" ng-controller="editarTragosController">
            <div class="block-header">
                <h2>Editar tragos</h2>
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>Lista de tragos</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>nombre</th>
                                            <th>valor</th>
                                            <th>Composicion</th>
                                            <th>Editar</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <tr ng-repeat="trago in tragos">
                                            <td>[[$index+1]]</td>
                                            <td>[[trago.trago.nombre]]</td>
                                            <td>[[trago.trago.precio]]</td>
                                            <td>
                                            

                      <script type="text/ng-template" id="menu">
    
                            <div class="ns-popover-tooltip" style="width:200px">
                              <ul>
                                <li ng-repeat="n in trago.ingredientes">[[n.nombre]] ([[n.ml]] ML)</li>
                              </ul>
                            </div>
                          </script>                      

                                    <button ns-popover
                                    ns-popover-template="menu"
                                    ns-popover-trigger="click"
                                    ns-popover-theme="ns-popover-list-theme"
                                    >
                              <i class="material-icons col-light-blue">add</i>
                            </button>



                               





                                            <!--
                                            -->
                                            </td>
                                            <td>
                                                <i ng-click="editar(trago)" class="material-icons col-light-blue">mode_edit</i>
                                                <i ng-click="delete(trago)" class="material-icons col-red">delete</i>
                                            </td>
                                        </tr>

                                    

                                        
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="header" ng-class="(edicion==true) ? 'bg-deep-orange' : 'bg-teal'">
                            <h2>[[textoAgregarEditar]]</h2>
                        </div>
                        <div class="body">
                            <form>
                                <div class="form-group">
                                    <label for="email">Nombre Trago:</label>
                                    <input ng-model="nuevoTrago.nombre" type="text" class="form-control" placeholder="Nombre" style="background-color: #e6e6e6;">
                                </div>

                                <div class="form-group">
                                    <label for="pwd">Valor:</label>
                                    <input ng-model="nuevoTrago.valor" type="number" placeholder="Valor" class="form-control" id="pwd" style="background-color: #e6e6e6;">
                                </div>

                                
                                <div class="form-group">
                                    <label for="sel1">Ingredientes:</label>
                                    <div ng-repeat="itemIngrediente in ingredientesTrago">
                                        [[itemIngrediente.nombre]] Ml: [[itemIngrediente.ml]] <i ng-click="deleteIngrediente(itemIngrediente)" class="material-icons col-red">delete</i>
                                    </div>
                                </div>
                                
                                <div class="form-group" ng-repeat = "ingrediente in nuevoTrago.ingredientes" >
                                                               
                                    <select  ng-model="ingrediente.id_ingrediente" class="col-md-8 col-xs-12 col-sm-12" id="sel1">
                                        <option ng-selected="item.nombre == ingrediente.nombre" ng-repeat="item in productos" value="[[item.id_producto]]">[[item.nombre]]</option>
                                    </select>
                                    
                                    <div class="col-md-4 col-xs-12 col-sm-12">
                                        <span class="col-md-2 col-xs-12 col-sm-12">Ml:</span>
                                        <input type="number" ng-model="ingrediente.ml" class="col-md-8 col-xs-12 col-sm-12">
                                    </div>
                                    

                                </div>

                                <div class="form-group" ng-click="nuevoIngrediente()" ng-hide="esCerveza">
                                    mas ingredientes: <i class="material-icons col-light-blue">add</i>
                                </div>
                                <button ng-click="save(nuevoTrago)" class="btn btn-default">[[textoAgregarEditar]]</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- #END# Browser Usage -->
            </div>
        </div>

@endsection
</html>