@extends('layouts.template');


@section('content')

    <div class="container-fluid" ng-controller="ventasPisoDosController">
            <div class="block-header">
                <h2>Mesas piso 2</h2>
            </div>

            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" ng-repeat="mesa in mesas">
                    <div class="info-box hover-expand-effect" ng-class="(mesa.estado==1) ? 'bg-orange' : 'bg-light-green'">
                        <div class="icon">
                        <!--<i>[[$index + 1]]</i>-->
                            <i>[[mesa.tituloMesa]]</i>
                        </div>
                        <div class="content">
                            <div class="text">[[mesa.nombre]] ($[[ mesa.total ]])</div>
                            <button type="button" style="z-index:9" class="btn btn-default waves-effect indexMil" data-toggle="modal" data-target="#largeModal" ng-click="mesaSeleccionada($index)">DETALLE</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->

        <!-- Large Size -->
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document" style="width:90% !important">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title text-center" id="largeModalLabel">Detalle mesa</h4>

                            <div class="form-group">
                                <label for="nombreProducto">Buscar producto:</label>
                                <angucomplete-alt id="ex1"
                                  placeholder="Nombre producto"
                                  pause="100"
                                  selected-object="productoSeleccionado"
                                  local-data="countries"
                                  search-fields="nombre"
                                  title-field="nombre"
                                  minlength="1"
                                  input-class="form-control form-control-small"/>      
                            </div>

                        </div>
                        <div class="modal-body">

                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 text-center">
                                <h4 class="text-center">Tragos</h4>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center" ng-repeat="trago in tragos" ng-click="addTrago(trago.trago)" >
                                    <i class="material-icons text-center">local_drink</i>
                                    <br>
                                        [[trago.trago.nombre]]
                                    <br>
                                        $[[trago.trago.precio]]
                                </div>

                            </div>
                            
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h4 class="text-center">Productos vendidos</h4>
                                <div class="modal-footer">
                                    <button ng-click="seleccionarTodos()" type="button" class="btn btn-link waves-effect" style="color: black;background: #cac9c9;">Seleccionar todos</button>    
                                </div>
                                <div ng-repeat="tragoMesa in mesas[indexMesaSeleccionada].tragosMesa">
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <i ng-click="deleteTrago(tragoMesa)" class="material-icons col-red">clear</i> [[tragoMesa.nombre]]
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                        <div class="demo-checkbox">
                                            <input type="checkbox" id="basic_checkbox_2[[$index]]" class="filled-in" ng-model="mesas[indexMesaSeleccionada].tragosMesa[ [[$index]] ].seleccionado">
                                            <label for="basic_checkbox_2[[$index]]"></label>
                                        </div>
                                    </div>

                                </div>
                                

                                
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                    <br>
                                    <strong>Total:</strong>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                    <br>
                                    <strong>$[[ mesas[indexMesaSeleccionada].total ]]</strong>
                                </div>
                            </div>
                            
                        </div>

                        <div class="modal-footer">

                            <button ng-click="imprimirBoleta( mesas[indexMesaSeleccionada] )" type="button" class="btn btn-link waves-effect" data-dismiss="modal">Imprimir boleta</button>

                             <button ng-click="checkOut( mesas[indexMesaSeleccionada] )" type="button" class="btn btn-link waves-effect" data-dismiss="modal">CheckOut</button>
                        </div>
                    </div>
                </div>
            </div>

    </div>

@endsection
</html>