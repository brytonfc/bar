@extends('layouts.template');


@section('content')
        
    <div class="container-fluid" ng-controller="iniciarCajaController">
            <div class="block-header">
                <h2>Iniciar Caja</h2>
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header" ng-class="(edicion==true) ? 'bg-deep-orange' : 'bg-teal'" ng-show="iniciarOk">
                            <h2>Caja iniciada</h2>                         
                        </div>
                        
                        <div class="body">
                            <form>
                                <div class="form-group">
                                    <label for="email">Monto:</label>
                                    <input ng-model="datos.monto" type="number" class="form-control" placeholder="monto" style="background-color: #e6e6e6;">
                                </div>
        
                                <button ng-click="save(datos)" class="btn btn-default">Iniciar caja</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                
                <!-- #END# Browser Usage -->
            </div>
        </div>

@endsection
</html>