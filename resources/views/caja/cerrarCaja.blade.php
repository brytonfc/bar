@extends('layouts.template');


@section('content')
        
    <div class="container-fluid" ng-controller="cerrarCajaController">
            <div class="block-header">
                <h2>Cerrar Caja</h2>
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">

                        <div class="header" ng-class="(edicion==true) ? 'bg-deep-orange' : 'bg-teal'" ng-show="cerrarOk">
                            <h2>Caja cerrada</h2>                         
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <button ng-click="save()" class="btn btn-default">Cerrar caja</button>
                        </div>

                        <div class="body">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                    <tr>
                                            <td>Caja Inicial</td>
                                            <td>$[[datos.cajaInicial |number|comma2decimal]]</td>
                                        </tr>  
                                        <tr>
                                            <td>Total Ventas</td>
                                            <td>$[[datos.ventas |number|comma2decimal]]</td>
                                        </tr>  
                                        <tr>
                                            <td>Total Caja</td>
                                            <td>$[[datos.totalDia |number|comma2decimal]]</td>
                                        </tr>  
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                
                <!-- #END# Browser Usage -->
            </div>
        </div>

@endsection
</html>