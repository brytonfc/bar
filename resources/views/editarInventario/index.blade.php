@extends('layouts.template');


@section('content')

    <div class="container-fluid" ng-controller="editarInventarioController">
        <div class="block-header">
            <h2>Productos</h2>
        </div>
    
    


        <!-- Basic Table -->
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Lista Prodcutos
                            </h2>
                        </div>
                        <div class="body table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nombre</th>
                                        <th>Se vende por unidad</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="producto in productos">
                                        <td class="row">[[$index + 1]]</td>
                                        <td >[[producto.nombre]]</td>
                                        <td ng-if="producto.se_vende_unidad == 1">Si</td>
                                        <td ng-if="producto.se_vende_unidad == 0">No</td>
                                        <td>
                                            <i ng-click="editar(producto)" class="material-icons col-light-blue">mode_edit</i>
                                            <i ng-click="delete(producto)" class="material-icons col-red">delete</i>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- #END# Basic Table -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="alert bg-green" ng-show="agregado">
                        Producto nuevo en Inventario, agregado correctamente.
                    </div>
                    <div class="card">
                        <div class="header" ng-class="(edicion==true) ? 'bg-deep-orange' : 'bg-teal'">
                            <h2>[[textoAgregarEditar]]</h2>
                        </div>
                        <div class="body">
                            <div class="form-group">
                                <label for="email">Nombre producto:</label>
                                <input ng-model="producto.nombre" type="text" class="form-control" placeholder="Nombre" style="background-color: #e6e6e6;">
                            </div>

                            <div class="form-group">
                                <label for="email">Producto se vende por unidad:</label>
                                <select ng-options="valor for valor in ['Si','No']" ng-model="producto.seVendeUnidad" class="form-control" ng-init="producto.seVendeUnidad = 'No'">
                                    
                                </select>


                            </div>
                            
                            <div class="form-group" ng-show="producto.seVendeUnidad == 'Si' ">
                               <input ng-model="producto.valor" type="number" class="form-control" placeholder="Precio" style="background-color: #e6e6e6;">
                            </div>

                            <div class="form-group">
                                <button ng-click="save(producto)" class="btn btn-default">[[textoAgregarEditar]]</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

@endsection
</html>