<!DOCTYPE html>
<html ng-app="bar">
<head>
    <title>Localhost</title>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Load Bootstrap CSS -->
        <link href="<?= asset('css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= asset('css/app.css') ?>" rel="stylesheet">

</head>
<body>
    <div class="main" ng-controller="loginController">
        <div class="login-form">
            <h1>NOMBRE BAR</h1>
                <div class="head">
                    <img src="img/user.png" alt=""/>
                </div>
                <form role="form" method="POST" action="<?= url('/login') ?>">
                    <input type="text" class="text" placeholder="Nombre Usuario" name="email">
                    <input type="password" placeholder="Cotnraseña" name="password">
                    <?php echo csrf_field(); ?>
                    <div class="submit">
                        <input type="submit">
                    </div>  
                </form>
            </div>
           
    </div>  

    <!-- Load Javascript Libraries (AngularJS, JQuery, Bootstrap) -->
        <script src="<?= asset('app/lib/angular/angular.min.js') ?>"></script>
        <script src="<?= asset('js/jquery.min.js') ?>"></script>
        <script src="<?= asset('js/bootstrap.min.js') ?>"></script>
        
        <!-- AngularJS Application Scripts -->
        <script src="<?= asset('app/app.js') ?>"></script>
        <script src="<?= asset('app/controllers/loginController.js') ?>"></script>             
</body>
</html>