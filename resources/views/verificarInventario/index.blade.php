@extends('layouts.template');


@section('content')
        
    <div class="container-fluid" ng-controller="verificarInventarioController">
            <div class="block-header">
                <h2>Verificar inventario</h2>
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header">

                            <h2>Cantidad productos</h2>

                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>nombre</th>
                                            <th>Cantidad (ml)</th>
                                            <th>Unidades</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <tr ng-repeat="item in stock">
                                            <td>[[$index+1]]</td>
                                            <td>[[item.nombre]]</td>
                                            <td>[[item.cantidad  |number|comma2decimal]]</td>
                                            <td>[[item.unidades |number|comma2decimal]]</td>
                                        </tr>  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                
                <!-- #END# Browser Usage -->
            </div>
        </div>

@endsection
</html>