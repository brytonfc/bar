<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trago;
use App\IngredientesTrago;

class TragosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id = null) {

        //return "H";
        
        if ($id == null) {

            $response = array();
            $tragos = Trago::all();
            $count = 0;

            

            foreach ($tragos as $trago) {
                $count++;
                //$response['tragos'] = $trago;
                $ingredientes = IngredientesTrago::select('producto.id_producto','nombre','ml','id_ingredientes_trago')->join('producto','ingredientes_trago.id_producto','=','producto.id_producto')
                ->where("id_trago","=",$trago['id_trago'])->get();

                //$ingredientes = DB::table('')
                $response[] = [
                    "trago"        => $trago,
                    "ingredientes" => $ingredientes
                ];
            }
            

            
            //$response['ingredientes'] = $ingredientes;

            return response()->json(["tragos"=>$response,"cont"=>$count]);
        } else {
            return $this->show($id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        //return $request->all();
        try{

            if($request->input("id_trago") == ""){
                $trago = new Trago;
                $accion = "guardar";
                }
            else
                {
                    $trago = Trago::find($request->input("id_trago"));
                    $accion = "editar";
                }


            
            $trago->nombre = $request->input('nombre');
            $trago->precio = $request->input("valor");
            $trago->save();
            $id = $trago->id_trago;
            //return $id;
            //return $request->input("ingredientes");
            if($request->input("ingredientes") != "")
            {
                foreach ($request->input("ingredientes") as $value) {
                    $ingrediente = new IngredientesTrago;
                    //return $value['ingrediente'];
                    $ingrediente->id_trago = $id;
                    $ingrediente->id_producto = $value['id_ingrediente'];
                    $ingrediente->ml = $value['ml'];
                    $ingrediente->save();
                }  
            }
            

            return response()->json(["resultado"=>"ok"]);
        }
        catch(\Exception $e){
            return response()->json(["resultado"=>"error","mensaje"=>$ex->getMessage()]);
        }
        
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */


    public function destroy(Request $request) {
        

        try{


            $id_trago = $request->input("trago")["id_trago"];
            //return $request->all();
            $trago = Trago::find($id_trago);

            $ingredientes = IngredientesTrago::where("id_trago","=",$trago->id_trago)->delete();
            
            
            

            $trago->delete();

            return response()->json(["resultado"=>"ok", "id_trago"=>$trago->id_trago]);
        }
        catch(\Exception $ex){
            return response()->json(["resultado"=>"error","mensaje"=>$ex->getMessage()]);
        }
    }


    public function borrarIngrediente(Request $request){

        $id_ingredientes_trago = $request->input("id_ingredientes_trago");

        IngredientesTrago::where("id_ingredientes_trago","=",$id_ingredientes_trago)->delete();

        return response()->json(["resultado"=>"ok"]);

    }
}
