<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mesa;

class MesasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id = null) {

        //return "H";
        $mesas = Mesa::all();
        
        if ($id == null) {

            return response()->json(["mesas"=>$mesas]);
        } else {
            return $this->show($id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        //return $request;

        //return $request->all();
        try{

        if($request->input("id_mesa") == ""){
            $mesa = new Mesa;
            $accion = "guardar";
        }
        else
        {
            $mesa = Mesa::find($request->input("id_mesa"));
            $accion = "editar";
        }
        //return $producto;

        //return $request->input("producto");
        $mesa->nombre = $request->input('nombre');
        $mesa->piso    = $request->input('piso');
        $mesa->titulo = $request->input('titulo');
        $mesa->save();

        return response()->json(["resultado"=>"ok", "id_mesa"=>$mesa->id_mesa, "accion"=>$accion]);
        }
        catch(\Exception $e){
            return response()->json(["resultado"=>"error","mensaje"=>$ex->getMessage()]);
        }
        
    }

  

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request) {
        
        //return $request->all();
        $mesa = Mesa::find($request->input('id_mesa'));

        $mesa->delete();

        return response()->json(["resultado"=>"ok"]);
        //return "Employee record successfully deleted #" . $request->input('id');
    }
}
