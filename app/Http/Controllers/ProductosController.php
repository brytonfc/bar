<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use App\Trago;
use App\IngredientesTrago;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id = null) {

        //return "H";
        
        if ($id == null) {

            return Producto::all();
        } else {
            return $this->show($id);
        }
    }

    public function ingredientes(){

        $productos = Producto::get();
        return response()->json(["resultado"=>"ok", "ingredientes"=>$productos]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        //return $request;

        //return $request->all();

        $id_producto  = $request->input("id_producto");
        $nombre         = $request->input('nombre');
        $seVendeUnidad  = $request->input("seVendeUnidad");
        $valor          = $request->input('valor');

        try{

        if($id_producto == ""){
            $producto = new Producto;
            $accion = "guardar";
        }
        else
        {
            $producto = Producto::find($id_producto);
            $accion = "editar";
        }
        //return $producto;

        //return $request->input("producto");
        $producto->nombre = $nombre;
        $producto->se_vende_unidad  = $seVendeUnidad;
        $producto->save();

        //si se vende por 
        if($seVendeUnidad == 1){
            $trago                  = new Trago;
            $trago->nombre          = $nombre;
            $trago->precio          = $valor;
            $trago->se_vende_unidad = 1;
            $trago->id_producto     = $producto->id_producto;
            $trago->save();

            $ingredienteTrago = new IngredientesTrago;
            $ingredienteTrago->id_producto= $producto->id_producto;
            $ingredienteTrago->id_trago = $trago->id_trago;
            $ingredienteTrago->ml =0;
            $ingredienteTrago->save();

        }



        return response()->json(["resultado"=>"ok", "id_producto"=>$producto->id_producto, "accion"=>$accion]);
        }
        catch(\Exception $ex){
            return response()->json(["resultado"=>$ex,"mensaje"=>$ex->getMessage()]);
        }
        
    }

  

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request) {
        
        //return $request->all();
        try{
            $producto = Producto::find($request->input('id_producto'));

            $producto->delete();

            return response()->json(["resultado"=>"ok"]);
            //return "Employee record successfully deleted #" . $request->input('id');
        }
        catch(\Exception $ex){

            return response()->json(["resultado"=>"error","mensaje"=>$ex->getMessage()]);
        }
        
    }
}
