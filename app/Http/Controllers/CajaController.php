<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mesa;
use App\HistorialMesa;
use App\IngredientesTrago;
use App\Stock;
use App\Trago;
use App\HistorialVenta;
use App\Caja;


class CajaController extends Controller
{

	
	public function iniciarCaja(Request $request){

		try{


		//verificar que no exista ninguna caja abierta pendiente
		$cantCajasAbiertas = Caja::whereNull("fecha_cierre_caja")->count();
		if($cantCajasAbiertas > 0){

			return response()->json(["resultado"=>"error", "mensaje"=>"error debe cerrar caja antes de abrir una nueva"]);
		}
		else{

			$carbon = new \Carbon\Carbon();
			$caja = new Caja;
			$caja->fecha_inicio_caja = $carbon->now();
			$caja->fecha_cierre_caja = NULL;
			$caja->valor = $request->monto;
			$caja->save();
			return response()->json(["resultado"=>"ok"]);
		}


		


		
		}
		catch(\Exception $ex){
			return response()->json(["resultado"=>"error","mensaje"=>$ex->getMessage()]);
		}


	}

	public function cerrarCaja(Request $request){

		try{


			$carbon = new \Carbon\Carbon();

			//busco la caja que tenga fecha de cierre null, es decir que este la caja abierta pero no cerrada
			$caja = Caja::whereNull("fecha_cierre_caja")->first();
			if($caja){
				$caja->fecha_cierre_caja = $carbon->now();
				$caja->save();

				//se suman los precios de venta entre las fechas de inicio y cierre de caja
				$sumaVentas = HistorialVenta::whereBetween("created_at",[$caja->fecha_inicio_caja,$caja->fecha_cierre_caja])->sum("precio");

				$sumaTotal = $sumaVentas + $caja->valor;
				$response = [
                    "ventas"        => $sumaVentas,
                    "cajaInicial"	=> $caja->valor,
                    "totalDia"		=> $sumaTotal
                    
                    
                ];

				return response()->json(["resultado"=>"ok", "ventas"=>$sumaVentas, "datos"=>$response]);
			}
			else{
				return response()->json(["resultado"=>"error", "mensaje"=>"Error, Asegurese de haber iniciado caja", "ventas"=>$caja]);
			}



			

			
            
		}
		catch(\Exception $ex){
			return response()->json(["resultado"=>"error","mensaje"=>$ex->getMessage()]);
		}


	}

	public function historialCaja(){

		try{
			$carbon = new \Carbon\Carbon();
			$cajas = Caja::get();


			foreach ($cajas as $caja) {

				//$carbon1 = Carbon::createFromDate($caja->fecha_inicio_caja);
				//$carbon1 = $carbon::createFromDate("d-m-Y",$caja->fecha_inicio_caja);
				$fecha_inicio_caja = new \Carbon\Carbon($caja->fecha_inicio_caja);
				$fecha_cierre_caja = new \Carbon\Carbon($caja->fecha_cierre_caja);
				$sumaVentas = HistorialVenta::whereBetween("created_at",[$caja->fecha_inicio_caja,$caja->fecha_cierre_caja])->sum("precio");

				$sumaTotal = $sumaVentas + $caja->valor;

				$response[] = [

					"inicio"    => $fecha_inicio_caja->format('d/m/Y H:i:s'),
					"cierre"    => $fecha_cierre_caja->format('d/m/Y H:i:s'),
					"valor"     => $caja->valor,
					"ventas"    => $sumaVentas,
					"sumaTotal" => $sumaTotal


                ];
			}
			
			return response()->json(["resultado"=>"ok","cajas"=>$response]);
		}
		catch(\Exception $ex){
			return response()->json(["resultado"=>"error","mensaje"=>$ex->getMessage()]);
		}
		
	}

}

?>