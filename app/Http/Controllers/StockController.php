<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id = null) {

        //return "H";
        
        if ($id == null) {

            return Stock::all();
        } else {
            return $this->show($id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {

        //return "HOLA";
        //return $request->all();
        try{

            if($request->input("id_stock") == ""){
                $stock = new Stock;
                $accion = "guardar";
            }
            else
            {
                $stock = Stock::find($request->input("id_stock"));
                $accion = "editar";
            }
            //return $producto;

            //return $request->input("producto");
            $stock->id_producto = $request->input('id_producto');
            $stock->ml          = $request->input('ml');
            $stock->cantidad    = $request->input('cantidad');
            $stock->save();

            return response()->json(["resultado"=>"ok", "id_stock"=>$stock->id_stock, "accion"=>$accion]);
        }
        catch(\Exception $ex){
            return response()->json(["resultado"=>"error","mensaje"=>$ex->getMessage()]);
        }
        
    }

 

 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request) {
        
        //return $request->all();
        $stock = Stock::find($request->input('id_stock'));

        $stock->delete();

        return response()->json(["resultado"=>"ok"]);
        //return "Employee record successfully deleted #" . $request->input('id');
    }


    /**
    
    Funcion para la vista VerificarInventario
    */
    public function verificarInventario()
    {
        //se vende por unidad
       $stockUnidad = Stock::selectRaw('producto.nombre,sum(cantidad) as sumaMl')->join('producto','producto.id_producto','stock.id_producto')->groupby('producto.nombre')->where("se_vende_unidad","=","1")->get();


       //se vende por ML
       $stockMl = Stock::selectRaw('producto.nombre,sum(ml*cantidad) as sumaMl')->join('producto','producto.id_producto','stock.id_producto')->groupby('producto.nombre')->where("se_vende_unidad","=","0")->get();


       foreach($stockUnidad as $unidades){

            $response[] = [
                    "nombre"        => $unidades->nombre." (".$unidades->ml." ml)",
                    "cantidad"      => "-",
                    "unidades"      => (int)$unidades->sumaMl
                    
                ];

       }

       foreach($stockMl as $mls){


            $response[] = [
                    "nombre"        => $mls->nombre." (".$mls->ml." ml)",
                    "cantidad"      => (int)$mls->sumaMl,
                    "unidades"      => "-"
                ];
       }



       

       return $response;
    }
}
