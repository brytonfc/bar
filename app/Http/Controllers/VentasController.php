<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mesa;
use App\HistorialMesa;
use App\IngredientesTrago;
use App\Stock;
use App\Trago;
use App\HistorialVenta;
use App\Producto;

class VentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id = null) {

        //return "H";
        $mesas = Mesa::all();
        
        if ($id == null) {

            return response()->json(["mesas"=>$mesas]);
        } else {
            return $this->show($id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */

    //AL APRETAR UN TRAGO LO GUARDA EN LA TABLA DE HISTORIAL MESA
    public function store(Request $request) {
        //return $request;

        //return $request->all();
        try{

        
            $historialMesa = new HistorialMesa;
            $accion = "guardar";
        
        //return $producto;

        //return $request->input("producto");
        $historialMesa->id_trago = $request->input('id_trago');
        $historialMesa->id_mesa    = $request->input('id_mesa');
        $historialMesa->save();

        return response()->json(["resultado"=>"ok", "id_historial_mesa"=>$historialMesa->id_historial_mesa, "accion"=>$accion]);
        }
        catch(\Exception $e){
            return response()->json(["resultado"=>"error","mensaje"=>$ex->getMessage()]);
        }
        
    }


    // OBTIENE TODOS LOS TRAGOS ASIGNADOS A LAS MESAS
    public function obtenerMesasTragos($piso){


        $mesas = Mesa::where("piso","=",$piso)->orderBy("titulo")->get();

        foreach ($mesas as $mesa) {

            $historialMesa = HistorialMesa::join('trago','historial_mesa.id_trago','=','trago.id_trago')->
            where("id_mesa","=",$mesa['id_mesa'])->get();

            

            $total = HistorialMesa::join('trago','historial_mesa.id_trago','=','trago.id_trago')->
            where("id_mesa","=",$mesa['id_mesa'])->sum('precio');

            if($total >0){
                $estado = 1;
            }
            else{
                $estado =0;
            }

            

            $response[] = [
                    "total"   => $total,
                    "estado"  => $estado,
                    "id_mesa" => $mesa['id_mesa'],
                    "nombre"  =>$mesa['nombre'],
                    "tituloMesa" => $mesa["titulo"],
                    "tragosMesa"  => $historialMesa,

                    
                ];
            
        }

        

        return response()->json(["mesas"=>$response]);
    }



    // AL FINALIZAR LA VENTA
    public function emitirBoleta(Request $request){
        try{

            //return $request->all();
            $resultado = "error";
            $id_mesa = $request->input('id_mesa');
            $tragosMesa = $request->input('tragosMesa');

            //return $tragosMesa;

            //return $tragosMesa[0]["seleccionado"];
            foreach ($tragosMesa as $trago) {

                if($trago["seleccionado"] == 'true'){
                    $resultado = "ok";
                    //return "aaa";
                    //Tragos de la mesa
                    $ingredientesTrago = ingredientesTrago::where("id_trago","=",$trago["id_trago"])->get();

                    foreach ($ingredientesTrago as $key => $producto) {
                        
                        //verifico si se debe descontar por ml o por unidad
                        $productoBD = Producto::where("id_producto","=",$producto["id_producto"])->first();

                        if($productoBD->se_vende_unidad == true){
                            $stock = new Stock;
                            $stock->id_producto = $producto["id_producto"];
                            $stock->ml  = 1;
                            $stock->cantidad = -1;
                            $stock->save();
                        }else{
                            $stock = new Stock;
                            $stock->id_producto = $producto["id_producto"];
                            $stock->ml  = $producto["ml"];
                            $stock->cantidad = -1;
                            $stock->save();

                        }

                        $historialMesaBorrar = HistorialMesa::where("id_mesa","=",$id_mesa)->where("id_historial_mesa","=",$trago["id_historial_mesa"])->delete();



                        
                    }

                    $precio = Trago::select("precio")->where("id_trago","=",$trago["id_trago"])->first();

                    $historialVenta = new HistorialVenta;
                    $historialVenta->id_stock = $stock->id_stock;
                    $historialVenta->id_trago = $trago["id_trago"];
                    $historialVenta->ml =0;  // creo que deberia borrar este campo en la BD
                    $historialVenta->precio = $precio->precio;
                    $historialVenta->save();
                    


                    
                    
                }

                
               
            }

            return response()->json(["resultado"=>$resultado,"tragosMesa"=>$tragosMesa]);

            

            /*
                Descontar STOCK
            */

                /*
            $historialMesa = HistorialMesa::where("id_mesa","=",$id_mesa)->get();

            //return $historialMesa;
            foreach ($historialMesa as $key => $tragos) {
                //return $tragos['id_trago'];


                // SI EL PRODUCTO SE VENDE POR ml SE DESCUENTA x ML Y LA CANTIDAD ES -1 PARA QUE AL MULTIPLICAR QUEDE EN NEGATIVO
                
                $ingredientesTrago = ingredientesTrago::where("id_trago","=",$tragos["id_trago"])->get();
                foreach ($ingredientesTrago as $key => $producto) {
                    //return $producto["id_producto"];
                    

                    $stock = new Stock;
                    $stock->id_producto = $producto["id_producto"];
                    $stock->ml  = $producto["ml"];
                    $stock->cantidad = -1;
                    $stock->save();
                }

                // SI EL PRODUCTO SE VENDE POR UNIDAD SE DESCUENTA 1 ML Y LA CANTIDAD ES -1 PARA QUE AL MULTIPLICAR QUEDE EN NEGATIVO
                $ventaUnidad = Trago::join('producto','producto.id_producto','=','trago.id_producto')->where("trago.se_vende_unidad","=","1")->where("trago.id_trago","=",$tragos["id_trago"])->get();

                foreach ($ventaUnidad as $producto) {

                    
                    $stock = new Stock;
                    $stock->id_producto = $producto["id_producto"];
                    $stock->ml  = 1;
                    $stock->cantidad = -1;
                    $stock->save();
                    
                }


                //GUARDAMOS EN LA TABLA DE HISTORIAL_VENTA EL TRAGO Y EL STOCK

                //obtengo el precio del trago
                $precio = Trago::select("precio")->where("id_trago","=",$tragos["id_trago"])->first();

                $historialVenta = new HistorialVenta;
                $historialVenta->id_stock = $stock->id_stock;
                $historialVenta->id_trago = $tragos["id_trago"];
                $historialVenta->ml =0;  // creo que deberia borrar este campo en la BD
                $historialVenta->precio = $precio->precio;
                $historialVenta->save();


            }
            

            $historialMesaBorrar = HistorialMesa::where("id_mesa","=",$id_mesa)->delete();

            return response()->json(["resultado"=>"ok", "historialMesa"=>$historialMesa, "ventaUnidad"=>$ventaUnidad]);
            */
        }
        catch(\Exception $ex){
            return response()->json(["resultado"=>"error","mensaje"=>$ex->getMessage()]);
        }
    }

  

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */

    //ELIMINA UN TRAGO DE LA MESA
    public function destroy(Request $request) {
        
        //return $request->all();
        $mesa = HistorialMesa::find($request->input('id_historial_mesa'));
        //return $mesa;
        $mesa->delete();

        return response()->json(["resultado"=>"ok"]);
        //return "Employee record successfully deleted #" . $request->input('id');
    }


    // CAMBIA DE MESA TODOS LOS TRAGOS
     
    public function cambiarMesa(Request $request){
        $idMesaActual = $request->input("mesaActual");
        $idMesaDestino = $request->input("mesaDestino");

        $historialMesa = HistorialMesa::where("id_mesa","=",$idMesaActual)->update(['id_mesa' => $idMesaDestino]);

        return response()->json(["resultado"=>"ok"]);
        //return redirect()->back();


    }


    public function informeVentasDiarias($desde = "null", $hasta = "null",$producto="null"){

        
        //return $producto;
        //$ventas = HistorialVenta::get();
        try{
            $response = [];
            $carbon = new \Carbon\Carbon();

            if($desde == "null"){
                $desde = $carbon->yesterday();
            }
            if($hasta == "null"){
                $hasta = $carbon->now();
            }



            $fecha_desde = new \Carbon\Carbon($desde);
            $fecha_hasta = new \Carbon\Carbon($hasta);

            $ventas = HistorialVenta::whereBetween("created_at",[$fecha_desde,$fecha_hasta])->get();

            if($producto != "null"){
                $ventas = $ventas->where("id_trago","=",$producto);
            }
            

            

            if($producto != "null"){
            $sumaVentas = HistorialVenta::whereBetween("created_at",[$fecha_desde,$fecha_hasta])->where("id_trago","=",$producto)->sum("precio");
            }
            else{
                $sumaVentas = HistorialVenta::whereBetween("created_at",[$fecha_desde,$fecha_hasta])->sum("precio");
            }

            foreach ($ventas as $venta) {

                        $trago = Trago::where("id_trago","=",$venta->id_trago)->first();
                        $fecha = $venta->created_at;

                        $response[] = [
                        "nombre"        => $trago->nombre,
                        "precio"      => $venta->precio,
                        "fecha"         => $fecha->format('d/m/Y H:i:s'),
                        "desde" => $fecha_desde,
                        "hasta" => $fecha_hasta,
                        
                        
                    ];
                        
                    }

                    //return $response;
                    return response()->json(["resultado"=>"ok", "datos"=>$response, "suma"=>$sumaVentas]);

        }
        catch(\Exception $ex){
            return response()->json(["resultado"=>"error","mensaje"=>$ex->getMessage()]);
        }

    

    }
}
