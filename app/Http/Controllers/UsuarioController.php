<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\User;



class UsuarioController extends Controller
{


/**
 * 
 */

    public function login(Request $request){

        
        //return $request->all();

        try{
            if (Auth::attempt(['name' => $request->username, 'password' => $request->password])) {
                //return "OK";
                //dd(Auth::check());
                Session::put('nombreUsuario', $request->username);
                Session::put('idUsuario', Auth::user()->id_usuario);
                return redirect('mesasPisoUno');

            }
            else
            {
                return redirect("login");
            }
        }
        catch(Exception $e){
            return $e;
        }
        
          
        //return "LO";
    }

    public function logout(){
        Auth::logout();
        Session::flush();
        return redirect("login");
    }

    public function cambiarClave(){
        return view('login/cambiarClave');
    }

    public function cambiarClavePost(Request $request){
        
        $usuario = User::find(Session::get("idUsuario"));
        $password = $request->input("nuevaClave");


        $usuario->password = bcrypt($password);
        $usuario->save();
        return "ok";
    }
}
